<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use App\Notifications\RegistrationUser;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/my-account';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'gender' => 'required|string',
            'birthday' => 'required|date',
            'twitter' => 'nullable|string',
            'facebook' => 'nullable|string',
            'instagram' => 'nullable|string',
            'role' => 'required|string',
            'country' => 'required|string',
            'state' => 'required|string',
            'city' => 'required|string',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'gender' => in_array($data['gender'], ['male', 'female']) ? $data['gender'] : 'male',
            'birthday' => $data['birthday'],
            'twitter' => $data['twitter'],
            'facebook' => $data['facebook'],
            'instagram' => $data['instagram'],
            'role' => $data['role'] == 'admin' ? 'listner' : $data['role'],
            'country' => $data['country'],
            'state' => $data['state'],
            'city' => $data['city'],
            'address' => $data['address'],
        ]);

        $user->notify(new RegistrationUser($user));
        return $user;
    }
    public static function upload(Request $request, string $folderSrc = '')
    {
        $avatar = $request->file('file')[0]->storePublicly($folderSrc);
        return str_replace($folderSrc . '/', '', $avatar);
    }

    /**
     * Store Avatar to database
     *id(user), file ,customName
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function uploadAvatar(Request $request)
    {
        $avatar = RegisterController::upload($request, 'public/avatars');
        User::query()->where('id',$request['id'])->update(['avatar'=> $avatar]);
        return redirect('my-account');
    }
}
