<?php

namespace App\Http\Controllers;

use App\Models\Genre;

class GenreController extends Controller
{
    public function products(int $id)
    {
        $media = Genre::query()->find($id)->media()->withCount('votes')->paginate(6);
        $genre = Genre::query()->pluck('name', 'id');
        return view('media.index', compact('media', 'genre'));
    }
}
