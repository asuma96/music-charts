<?php

namespace App\Http\Controllers;

use App\Http\Requests\MediaRequest;
use App\Models\Genre;
use App\Models\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MediaController extends Controller
{
    public function index(Request $request)
    {
        $media = Media::query()->where('approved', '1')->searchable($request)
            ->withCount('votes')->orderBy('votes_count', 'desc')->paginate(6);
        $genre = Genre::query()->pluck('name', 'id');
        return view('media.index', compact('media', 'genre'));
    }

    public function create()
    {
        $genres = Genre::query()->pluck('name', 'id');
        $user = Auth::user()->with(['media'])->withCount(['followers', 'followingers', 'votes'])
            ->find(Auth::user()->id);
        return view('media.create', compact('user', 'genres'));
    }

    public function store(MediaRequest $request)
    {
        $request['link_to_track'] = $this->cutFrame($request->link_to_track);
        $media = Auth::user()->media()->create($request->except('genre_list'));
        $media->genres()->sync($request->input('genre_list'));
        return redirect()->back()->with(['message' => 'You media add success']);
    }

    public function likes()
    {
        $media = Media::query()->where('approved', '1')->withCount('votes')
            ->orderBy('votes_count', 'desc')->paginate(6);
        $genre = Genre::query()->pluck('name', 'id');
        return view('media.index', compact('media', 'genre'));
    }

    public function show(int $id)
    {
        $media = Media::query()->with(['genres'])->withCount('votes')->find($id);
        return view('media.show', compact('media'));
    }

    public function search(Request $request)
    {
        $media = Media::query()->where('approved', '1')->where('title', 'like', "%{$request->search}%")
            ->withCount('votes')->paginate(10);
        return view('media.index', compact('media'));
    }

    public function edit(Media $media)
    {
        $genres = Genre::query()->pluck('name', 'id');
        $user = Auth::user()->with(['media'])->withCount(['followers', 'followingers', 'votes'])
            ->find(Auth::user()->id);
        return view('media.edit', compact('media', 'user', 'genres'));
    }

    public function update(MediaRequest $request, Media $media)
    {
        $request['link_to_track'] = $this->cutFrame($request->link_to_track);
        $media->update($request->except('genre_list'));
        $media->genres()->sync($request->input('genre_list'));
        return redirect()->back()->with(['message' => 'You media update success']);
    }

    public function destroy(Media $media)
    {
        if (Auth::user()->media()->where('id', $media->id)->count() > 0) {
            $media->delete();
        }
        return redirect()->back()->with('message', 'You record delete success');
    }

    /**
     * @param $link
     * @return string
     */
    private function cutFrame(string $link)
    {
        if (strpos($link, 'tracks/', 1) == false) {
            return $link;
        }
        $start = strpos($link, 'tracks/', 1)+7;
        $end = strpos($link, '&amp;', 1)-$start;
        return substr($link, $start, $end);
    }
}
