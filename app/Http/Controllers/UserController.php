<?php

namespace App\Http\Controllers;

use App\Models\Genre;
use App\Models\Media;
use App\Models\Notification;
use App\Models\User;
use App\Notifications\FavoriteMedia;
use App\Notifications\FollowFromUser;
use App\Notifications\VoteUser;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Auth\RegisterController;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only(['favorites', 'myAccount', 'activity']);
    }

    public function artists()
    {
        $users = User::entity('artists')->paginate(6);
        return view('users.index', compact('users'));
    }

    public function produsers()
    {
        $users = User::entity('produsers')->paginate(6);
        return view('users.index', compact('users'));
    }

    public function show(int $id)
    {
        $user = User::query()->withCount('media', 'followers', 'followingers', 'votes')->find($id);
        $messages = $user->messages($user->id, Auth::user()->id)
            ->selectRaw('messages.sender_id, messages.resiver_id, users.name, messages.text, messages.created_at')
            ->orderBy('messages.created_at', 'desc')->get();
        $user->updateMessages(Auth::user()->id);
        return view('users.show', compact('user', 'messages'));
    }

    public function favoritesList()
    {
        $genre = Genre::query()->pluck('name', 'id');
        $media = Auth::user()->favorites()->withCount('votes')->paginate(6);
        return view('media.index', compact('media', 'genre'));
    }

    public function myAccount()
    {
        $user = Auth::user()->with(['media'])->withCount(['followers', 'followingers', 'votes'])
            ->find(Auth::user()->id);
        $media = Media::query()->where('approved', '1')->withCount('votes')->orderBy('created_at', 'desc')->paginate(6);
        return view('users.my-account', compact('user', 'media'));
    }

    public function activity()
    {
        $user = Auth::user()->with(['media'])->withCount(['followers', 'followingers', 'votes'])
            ->find(Auth::user()->id);
        $notifications = Notification::query()->where('created_at', '>=', Carbon::yesterday())
            ->orderBy('created_at', 'desc')->paginate(20);
        return view('users.activity', compact('user', 'notifications'));
    }

    public function search(Request $request)
    {
        $users = User::query()->where('name', 'like', "%{$request->search}%")
            ->withCount('media')->withCount('followers')->paginate(6);
        return view('users.index', compact('users'));
    }

    public function sync(string $relation, int $id, string $action = 'attach')
    {
        $action == 'attach' ? Auth::user()->$relation()->attach($id) : Auth::user()->$relation()->detach($id);
        $this->sendNotify($relation, $id, $action);
        return redirect()->back();
    }

    private function sendNotify($relation, $id, $action)
    {
        switch ($relation) {
            case 'votes':
                Auth::user()->notify(new VoteUser(Media::query()->find($id), $action));
                break;
            case 'followingers':
                Auth::user()->notify(new FollowFromUser(User::query()->find($id), $action));
                break;
            case 'favorites':
                Auth::user()->notify(new FavoriteMedia(Media::query()->find($id), $action));
                break;
        }
    }

    public function myMedia()
    {
        $media = Auth::user()->media()->paginate(10);
        $user = Auth::user()->with(['media'])->withCount(['followers', 'followingers', 'votes'])
            ->find(Auth::user()->id);
        return view('users.my-media', compact('user', 'media'));
    }

    public function topVoters(Request $request)
    {
        $date = self::getDate($request->top);
        $users = User::query()->withCount(['votes' => function ($q) use ($date) {
            if (!is_null($date)) {
                $q->where('votes.created_at', '>=', $date)
                    ->where('votes.created_at', '<=', Carbon::now());
            }
        }])->orderBy('votes_count', 'desc')->paginate(6);
        return view('users.top-voters', compact('users'));
    }

    /**
     * @param $top
     * @return null|Carbon
     */
    public static function getDate($top)
    {
        $date = null;
        if (!empty($top)) {
            if ($top == 'day') {
                $date = Carbon::now()->subDay();
            }
            if ($top == 'week') {
                $date = Carbon::now()->subDays(7);
            }
            if ($top == 'month') {
                $date = Carbon::now()->subDays(31);
            }
        }
        return $date;
    }

    /**
     * @param Request $request
     * @param User $user - resiver
     */
    public function sendMessage(Request $request, User $user)
    {
        Auth::user()->sender()->attach([$user->id => ['text' => $request->text, 'created_at' => Carbon::now()]]);
        return back();
    }

    protected function unreadMessage()
    {
//        $messages_new = Auth::user()->resiver()
//            ->selectRaw('count(messages.sender_id) as mysum')
//            ->wherePivot('read_at', '=', null)->orderBy('mysum', 'desc')
//            ->groupBy('messages.sender_id')->paginate(10);

        $user_id = Auth::user()->id;
        $messages = DB::select(
            "SELECT
              messages.sender_id,
              messages.resiver_id,
              messages.text,
              messages.created_at,
              if(resiver.id = $user_id, sender.name, resiver.name) as recive_name,
              if(resiver.id = $user_id, sender.id, resiver.id) as recive_id,
              sum(if(IFNULL(read_at, 1) = 1, 1, 0)) as count_not_read
            FROM messages RIGHT JOIN users resiver ON messages.resiver_id = resiver.id RIGHT JOIN users sender ON messages.sender_id = sender.id
            WHERE messages.resiver_id = $user_id OR messages.sender_id = $user_id
            GROUP BY recive_id
            ORDER BY messages.created_at DESC"
        );
//          dd($messages);
        return view('users.messages', compact('messages_new', 'messages'));
    }

    /**
     * Edit avatar
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Request $request)
    {
        $media = Auth::user()->media()->paginate(10);
        if (Hash::check($request['last_password'], Auth::user()->password)) {
            if ($request['password'] == "" || $request['password'] == null) {
                User::query()->where('id', Auth::user()->id)->update($request->except('_token', 'last_password', 'password_confirmation','password'));
                $user = User::query()->where('id', Auth::user()->id)->get();
                return view('users.my-account', compact('user', 'media'));
            }
            else
            {
                $password=bcrypt($request['password']);
                User::query()->where('id', Auth::user()->id)->update($request->except('_token','password', 'last_password', 'password_confirmation','password'));
                User::query()->where('id', Auth::user()->id)->update(['password'=>$password]);
                                $user = User::query()->where('id', Auth::user()->id)->get();
                return view('users.my-account', compact('user', 'media'));
            }
        }
        return ('default value');

    }

    public function editProfile()
    {
        $user = Auth::user();
        $user->select(DB::raw('DATE_FORMAT(birthday, "%Y/%d/%m") as time'))->get();
        $user->save();
        return view('users.edit', compact('user'));
    }

    /**
     * Edit avatar
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function uploadAvatar(Request $request)
    {
        $avatar = RegisterController::upload($request, 'public/avatars');
        Auth::user()->update(['avatar' => $avatar]);
        return view('users.my-account', compact('user'));
    }
}
