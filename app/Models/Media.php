<?php

namespace App\Models;

use App\Http\Controllers\UserController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;

class Media extends Model
{
    use SoftDeletes, Notifiable;

    protected $fillable = [
        'user_id',
        'title',
        'year',
        'type',
        'extension',
        'size',
        'duration',
        'approved',
        'link_to_track',
    ];

    protected $with = ['author'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function genres()
    {
        return $this->belongsToMany(Genre::class, 'media_genre', 'media_id', 'genre_id');
    }

    public function favoritesUser()
    {
        return $this->belongsToMany(User::class, 'favorites');
    }

    public function votes()
    {
        return $this->belongsToMany(User::class, 'votes')->withTimestamps();
    }

    public function getGenreListAttribute()
    {
        return $this->genres->pluck('id')->toArray();
    }

    public function scopeSearchable($q, Request $request)
    {
        if (!empty($request->type)) {
            $q->whereType($request->type);
        }
        if (!empty($request->created_at) || ($request->created_at = 'all')) {
            $date = UserController::getDate($request->created_at);
            if (!is_null($date)) {
                $q->where('created_at', '>=', $date);
            }
        }
        if (!empty($request->genre)) {
            $q->whereHas('genres', function ($query) use ($request) {
                $query->whereIn('id', $request->genre);
            });
        }
        return $q;
    }
}
