<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;
    protected $fillable = [
        'name',
        'email',
        'password',
        'gender',
        'birthday',
        'twitter',
        'facebook',
        'instagram',
        'role',
        'country',
        'state',
        'city',
        'address',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ['birthday', 'created_at', 'updated_at', 'deleted_at'];

    public function media()
    {
        return $this->hasMany(Media::class);
    }

    public function sender()
    {
        return $this->belongsToMany(User::class, 'messages', 'sender_id', 'resiver_id');
    }

    public function resiver()
    {
        return $this->belongsToMany(User::class, 'messages', 'resiver_id', 'sender_id');
    }

    public function listMessages($user_id)
    {
      return $this->belongsToMany(User::class, 'messages', 'resiver_id', 'sender_id')
        ->orWherePivot('sender_id', '=', $user_id)
        ->orWherePivot('resiver_id', '=', $user_id)
        ->withPivot('text')
        ->groupBy('resiver_id')
        ->withTimestamps();
    }

    public function messages($sender_id, $resiver_id)
    {
        return $this->belongsToMany(User::class, 'messages', 'resiver_id', 'sender_id')
            ->wherePivot('sender_id', '=', $resiver_id)
            ->orWherePivot('resiver_id', '=', $resiver_id)
            ->wherePivot('sender_id', '=', $sender_id)
            ->withPivot('text')
            ->withTimestamps();
    }

    public function updateMessages($resiver_id)
    {
        return $this->belongsToMany(User::class, 'messages', 'sender_id', 'resiver_id')
            ->wherePivot('read_at', '=', null)
            ->updateExistingPivot($resiver_id, ['read_at' => Carbon::now()]);
    }

    public function followers()
    {
        return $this->belongsToMany(User::class, 'followers', 'follower_id', 'following_id');
    }

    public function followingers()
    {
        return $this->belongsToMany(User::class, 'followers', 'following_id', 'follower_id');
    }

    public function votes()
    {
        return $this->belongsToMany(Media::class, 'votes')->withTimestamps()->withPivot('date_likes');
    }

    public function todayVotes()
    {
        return $this->belongsToMany(Media::class, 'votes')->withTimestamps()->withPivot('date_likes')
            ->wherePivot('date_likes', '=', Carbon::now()->format('Y-m-d'));
    }

    public function favorites()
    {
        return $this->belongsToMany(Media::class, 'favorites');
    }

    public function scopeEntity($q, string $entity)
    {
        if ($entity == 'artists') {
            return $this->scopeArtists($q);
        }
        return $this->scopeProdusers($q);
    }

    private function scopeArtists($q)
    {
        return $q->whereRole('artist')->withCount('media')->withCount('followers');
    }

    private function scopeProdusers($q)
    {
        return $q->whereRole('produser')->withCount('media')->withCount('followers');
    }
}
