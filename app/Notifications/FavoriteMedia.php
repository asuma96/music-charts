<?php

namespace App\Notifications;

use App\Models\Media;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class FavoriteMedia extends Notification
{
    use Queueable;

    protected $media;
    protected $action;

    public function __construct(Media $media, $action)
    {
        $this->media = $media;
        $action == 'attach' ? $this->action = 'add to favorites' : $this->action = 'remove from favorites';
    }

    public function via($notifiable)
    {
        return ['database'];
    }

    public function toArray($notifiable)
    {
        return [
            'user' => $notifiable,
            'media' => $this->media,
            'action' => $this->action,
        ];
    }
}
