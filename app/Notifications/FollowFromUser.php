<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class FollowFromUser extends Notification
{
    use Queueable;

    protected $user;
    protected $action;

    public function __construct(User $user, $action)
    {
        $this->user = $user;
        $action == 'attach' ? $this->action = 'follow to' : $this->action = 'unfollow from';
    }

    public function via($notifiable)
    {
        return ['database'];
    }

    public function toArray($notifiable)
    {
        return [
            'whoFollow' => $notifiable,
            'followUser' => $this->user,
            'action' => $this->action,
        ];
    }
}
