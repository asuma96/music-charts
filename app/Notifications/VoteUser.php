<?php

namespace App\Notifications;

use App\Models\Media;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class VoteUser extends Notification
{
    use Queueable;

    protected $media;
    protected $action;

    public function __construct(Media $media, string $action)
    {
        $this->media = $media;
        $action == 'attach' ? $this->action = 'set like' : $this->action = 'reset like';
    }

    public function via($notifiable)
    {
        return ['database'];
    }

    public function toArray($notifiable)
    {
        return [
            'user' => $notifiable,
            'media' => $this->media,
            'action' => $this->action,
        ];
    }
}
