<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->userName,
        'email' => $faker->unique()->email,
        'password' => bcrypt('111111'),
        'gender' => $faker->randomElement(['male', 'female']),
        'birthday' =>$faker->dateTimeBetween('-60 years', '-20 years')->format('Y-m-d'),
        'twitter' => 'https://twitter.com/'.$faker->userName,
        'facebook' => 'https://www.facebook.com/'.$faker->userName,
        'instagram' => 'https://www.instagram.com/'.$faker->userName,
        'role' => $faker->randomElement(['listner', 'artist', 'produser', 'admin']),
        'country' => $faker->country,
        'state' => $faker->name,
        'city' => $faker->city,
        'address' => $faker->streetAddress,
        'created_at' => $faker->dateTimeBetween('-2 days'),
        'avatar'=>$faker->image($dir = '/tmp', $width = 640, $height = 480)
    ];
});

$factory->define(App\Models\Genre::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
    ];
});

$factory->define(App\Models\Media::class, function (Faker\Generator $faker) {
    $authors = \App\Models\User::query()->where('role', 'artist')->pluck('id')->toArray();
    return [
        'user_id' => $faker->randomElement($authors),
        'title' => $faker->unique()->text(16),
        'approved' => (int)$faker->boolean(80),
        'link_to_track' => $faker->url
    ];
});
