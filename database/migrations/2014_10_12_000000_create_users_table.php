<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->index()->unique();
            $table->string('password');
            $table->enum('gender', ['male', 'female'])->default('male');
            $table->date('birthday')->nullable();
            $table->string('twitter')->nullable();
            $table->string('facebook')->nullable();
            $table->string('instagram')->nullable();
            $table->enum('role', ['listner', 'artist', 'produser', 'admin']);
            $table->string('country')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->string('address')->nullable();
            $table->string('avatar')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('followers', function (Blueprint $table) {
            $table->integer('follower_id')->unsigned()->index();
            $table->foreign('follower_id')->references('id')->on('users')->onDelete('CASCADE');

            $table->integer('following_id')->unsigned()->index();
            $table->foreign('following_id')->references('id')->on('users')->onDelete('CASCADE');

            $table->unique(['follower_id', 'following_id'], 'follower_following_index');
        });

        Schema::create('messages', function (Blueprint $table) {
            $table->integer('sender_id')->unsigned()->index();
            $table->foreign('sender_id')->references('id')->on('users')->onDelete('CASCADE');

            $table->integer('resiver_id')->unsigned()->index();
            $table->foreign('resiver_id')->references('id')->on('users')->onDelete('CASCADE');

            $table->text('text');
            $table->timestamps();
            $table->timestamp('read_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
        Schema::dropIfExists('followers');
        Schema::dropIfExists('users');
    }
}
