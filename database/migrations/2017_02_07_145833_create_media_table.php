<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->nullable()->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('SET NULL');

            $table->string('title');
            $table->boolean('approved')->default(0);
            $table->string('link_to_track');
            $table->timestamps();
            $table->softDeletes();
        });


        // likes
        Schema::create('votes', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE');

            $table->integer('media_id')->unsigned()->index();
            $table->foreign('media_id')->references('id')->on('media')->onDelete('CASCADE');

            $table->date('date_likes')->default(DB::select('SELECT CURDATE() as now')[0]->now);

            $table->unique(['user_id', 'media_id', 'date_likes'], 'user_media_index');

            $table->timestamps();
        });

        // wishlist
        Schema::create('favorites', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE');

            $table->integer('media_id')->unsigned()->index();
            $table->foreign('media_id')->references('id')->on('media')->onDelete('CASCADE');

            $table->unique(['user_id', 'media_id'], 'user_media_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('favorites');
        Schema::dropIfExists('votes');
        Schema::dropIfExists('media');
    }
}
