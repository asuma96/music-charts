<?php

use Illuminate\Database\Seeder;

class GenreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Genre::query()->create([
            'name' => 'rock'
        ]);
        \App\Models\Genre::query()->create([
            'name' => 'pop'
        ]);
        \App\Models\Genre::query()->create([
            'name' => 'dance'
        ]);
        \App\Models\Genre::query()->create([
            'name' => 'metal'
        ]);
        \App\Models\Genre::query()->create([
            'name' => 'chill-out'
        ]);
        \App\Models\Genre::query()->create([
            'name' => 'rap'
        ]);
        \App\Models\Genre::query()->create([
            'name' => 'trance'
        ]);
    }
}
