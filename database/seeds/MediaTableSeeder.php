<?php

use Illuminate\Database\Seeder;

class MediaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $medias = factory(\App\Models\Media::class, 100)->create();
        $faker = Faker\Factory::create();
        $genreMin = \App\Models\Genre::query()->first()->id;
        $genreMax = \App\Models\Genre::query()->count();
        $userMin = \App\Models\User::query()->first()->id;
        $userMax = \App\Models\User::query()->count();
        foreach ($medias as $media) {
            $media->genres()->attach(rand($genreMin, $genreMax));
            $media->votes()->sync(
                $faker->randomElements(
                    [
                        rand($userMin, $userMax),
                        rand($userMin, $userMax),
                        rand($userMin, $userMax),
                        rand($userMin, $userMax),
                        rand($userMin, $userMax),
                        rand($userMin, $userMax),
                        rand($userMin, $userMax),
                        rand($userMin, $userMax),
                        rand($userMin, $userMax),
                        rand($userMin, $userMax)
                    ],
                    rand(1, 10)
                )
            );
            $media->favoritesUser()->sync(
                $faker->randomElements(
                    [
                    rand($userMin, $userMax),
                    rand($userMin, $userMax),
                    rand($userMin, $userMax),
                    rand($userMin, $userMax),
                    rand($userMin, $userMax),
                    rand($userMin, $userMax),
                    rand($userMin, $userMax),
                    rand($userMin, $userMax),
                    rand($userMin, $userMax),
                    rand($userMin, $userMax)
                    ],
                    rand(1, 10)
                )
            );
        }
    }
}
