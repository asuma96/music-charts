<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\User::class, 50)->create();
        $faker = Faker\Factory::create();
        $users = \App\Models\User::query()->get();
        $userMin = \App\Models\User::query()->first()->id;
        $userMax = \App\Models\User::query()->count();
        foreach ($users as $user) {
            $user->resiver()->attach([
                rand(1, $userMax) => [
                    'text' => $faker->text(rand(20, 120)), 'created_at'=> $faker->dateTimeBetween('-1 day', '+1 day')
                ]
            ]);
            $user->resiver()->attach([
                rand(1, $userMax) => [
                    'text' => $faker->text(rand(20, 120)), 'created_at'=> $faker->dateTimeBetween('-1 day', '+1 day')
                ]
            ]);
            $user->resiver()->attach([
                rand(1, $userMax) => [
                    'text' => $faker->text(rand(20, 120)), 'created_at'=> $faker->dateTimeBetween('-1 day', '+1 day')
                ]
            ]);
            $user->resiver()->attach([
                rand(1, $userMax) => [
                    'text' => $faker->text(rand(20, 120)), 'created_at'=> $faker->dateTimeBetween('-1 day', '+1 day')
                ]
            ]);
            $user->resiver()->attach([
                rand(1, $userMax) => [
                    'text' => $faker->text(rand(20, 120)), 'created_at'=> $faker->dateTimeBetween('-1 day', '+1 day')
                ]
            ]);
            $user->resiver()->attach([
                rand(1, $userMax) => [
                    'text' => $faker->text(rand(20, 120)), 'created_at'=> $faker->dateTimeBetween('-1 day', '+1 day')
                ]
            ]);
            $user->resiver()->attach([
                rand(1, $userMax) => [
                    'text' => $faker->text(rand(20, 120)), 'created_at'=> $faker->dateTimeBetween('-1 day', '+1 day')
                ]
            ]);
            $user->followers()->sync(
                $faker->randomElements(
                    [
                        rand($userMin, $userMax),
                        rand($userMin, $userMax),
                        rand($userMin, $userMax),
                        rand($userMin, $userMax)
                    ],
                    rand(1, 4)
                )
            );
        }
    }
}
