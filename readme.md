# Laravel PHP Framework

## How use
1) Copy .env.example to .env

2) Create database and user for database

3) Write needed data to .env file

4) Open folder with project in terminal, and write one command: ***php artisan migrate --seed***

5) Run command **php artisan serve** and open your site to [http://127.0.0.1:8000](http://127.0.0.1:8000)

Enjoy :)