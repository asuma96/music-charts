require('./bootstrap')
// require('./main')

import Vue from 'vue'

Vue.component('example', require('./components/Example.vue'))
// Vue.component('filter', require('./components/Filter.vue'))

new Vue({
  el: '#app'
})
