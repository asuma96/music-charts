import $ from 'jquery'
import BootstrapDialog from 'bootstrap3-dialog'



$('.modal-delete').click(function () {
  let id = $(this).attr('data-delete-id');
  BootstrapDialog.show({
    message: 'Hi Apple! <input type="hidden" name="_token" value="{{ csrf_token() }}">',
    buttons: [{
      label: 'Ok',
      action: function(){
        $.ajax({
          url: '/media/' + id,
          type: 'DELETE',
          success: function(result) {
            // Do something with the result
          }
        });
      }
    }, {
      label: 'Cancel',
      action: function(dialogItself){
        dialogItself.close();
      }
    }]
  });
})
