<!DOCTYPE html>
<html>
<head>
    <title>Be right back.</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            color: #999;
            display: table;
            font-weight: 100;
            font-family: 'Lato', sans-serif;
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 12vw;
            margin-bottom: 40px;
        }

        .link {
            font-size: 3vw;
            margin-bottom: 40px;
            color: #990073;
        }

        .link a {
            text-decoration: none

        }

    </style>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="title">404 Not found</div>
        <div class="link"><a href="/my-account">Click on the label for Avto visit your pages</a></div>
    </div>
</div>
</body>
</html>
