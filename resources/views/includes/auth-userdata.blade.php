<div class="col-md-4">
  <div class="user-avatar">
    <?php $user = Auth::user()?>
    @if ($user->avatar)
        {{--<img src="storage/app/public/avatar/{{ $user->avatar }}">--}}
      @else
{{--<p>kokokoko</p>--}}
    @endif
  </div>
  <div class="col-xs-6">
    <div class="user-name">{{ $user->name }}</div>
    <div class="user-speciality">{{ $user->role }}</div>
  </div>
  <div class="col-xs-6">

    @if(Auth::user()->id != $user->id)
      <?php $follow = Auth::user()->followingers()->pluck('id')->toArray() ?>
      @if(!in_array($user->id, $follow))
        <a class="btn btn-default follow btn-action" href="/followingers/{{ $user->id }}/attach">
          <span><i class="glyphicon glyphicon-user" aria-hidden="true"></i> Follow</span>
        </a>
      @else
        <a class="btn btn-default follow" href="/followingers/{{ $user->id }}/detach">
          <span><i class="glyphicon glyphicon-user" aria-hidden="true"></i> Unfollow</span>
        </a>
      @endif
    @endif


    {{--<button type="button" class="btn btn-default btn-lg follow">--}}
    {{--<span class="glyphicon glyphicon-user" aria-hidden="true"><b> Follow</b></span>--}}
    {{--</button>--}}
  </div>

  <div class="col-xs-12">
    <div class="user-info">
      <div class="user-account-type"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>
        <div>STAFF</div>
      </div>
      <div class="user-marker-map"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
        <div>{{ $user->country }}, {{ $user->state }}, {{ $user->city }}</div>
      </div>
      <div class="user-email"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
        {{ $user->email }}
      </div>

      @if($user->twitter != null)
        <div class="user-link"><span class="glyphicon glyphicon-link" aria-hidden="true"></span>
          {{ Html::link($user->twitter)}}
        </div>
      @endif
      @if($user->instagram != null)
        <div class="user-link"><span class="glyphicon glyphicon-link" aria-hidden="true"></span>
          {{ Html::link($user->instagram)}}
        </div>
      @endif
      @if($user->facebook != null)
        <div class="user-link"><span class="glyphicon glyphicon-link" aria-hidden="true"></span>
          {{ Html::link($user->facebook)}}
        </div>
      @endif
      <div class="user-joined"><span class="glyphicon glyphicon-time" aria-hidden="true"></span>
        <div>Joined on {{ date_format($user->created_at, 'M j, Y') }}</div>
      </div>
    </div>
  </div>
  <div class="col-xs-12">
    <div class="user-followers">
      <div class="col-xs-4">
        <span>{{ $user->followers_count }}</span><br/>
        <p>followers</p>
      </div>
      <div class="col-xs-4">
        <span>{{ $user->followingers_count }}</span><br/>
        <p>following</p>
      </div>
      <div class="col-xs-4">
        <span>{{ $user->votes_count }}</span><br/>
        <p>votes</p>
      </div>
    </div>
  </div>

</div>


