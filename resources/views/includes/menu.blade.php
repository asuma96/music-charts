<header role="header">
  <div class="container">
    <div class="col">
      <div class="col-md-12 bg-black">
        <div class="row">
          <div class="col-md-2 logo"></div>
          <div class="col-md-10 nav-header">
            <ul class="nav navbar-nav nav-link">
              <li>{{ Html::link('/media/create', 'SUBMIT SONG', ['class'=> Request::is('media/create') ? 'active' : ''])}}</li>
              <li>{{ Html::link('/unread', 'MESSAGES', ['class'=> Request::is('unread') ? 'active' : ''])}}</li>
              <li>{{ Html::link('/media', 'CHARTS', ['class'=> Request::is('media') ? 'active' : ''])}}</li>
              <li>{{ Html::link('/top-voters', 'FAVORITES', ['class'=> Request::is('top-voters') ? 'active' : ''])}}</li>
              <li>{{ Html::link('/', 'MAIN SITE', ['class'=> Request::is('/') ? 'active' : ''])}}</li>
            </ul>
            <form class="navbar-form navbar-left" role="search" method="get" action="/search">
              <div class="form-group">
                <input type="text" class="form-control input" placeholder="author/produser search" name="search">
              </div>
            </form>
            <ul class="nav navbar-nav navbar-right">
              @if (Auth::guest())
                <li><a href="{{ route('login') }}">Login</a></li>
                <li><a href="{{ route('register') }}">Register</a></li>
              @else
                <li class="dropdown nav-link">
                  <a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true"
                     aria-expanded="false">
                    <div class="small-avatar"><img src="/img/avatar-1.jpg" alt=""></div>
                    {{ Auth::user()->name }}
                    <span class="caret"></span></a>
                  <ul class="dropdown-menu border-radius-0">
                    <li>{{ Html::link('/my-account', 'My account')}}</li>
                    <li>{{ Html::link('/favorites', 'Favorites')}}</li>
                    <li>{{ Html::link('/my-media', 'My medias')}}</li>
                    <li>{{ Html::link('/edit', 'Edit Profile')}}</li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{{ route('logout') }}"
                           onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        Logout
                      </a></li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                          style="display: none;">
                      {{ csrf_field() }}
                    </form>
                  </ul>
                </li>
              @endif
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</header>
