@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title-page">
          ADD MEDIA
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        @include('includes.auth-userdata')
        <div class="col-md-8">
          @include('includes.tab-menu')
          @include('message')
          {!! Form::open(['url' => '/media']) !!}
          @include('media.form', ['btnText' => 'Create media'])
          {!! Form::close() !!}

        </div>
      </div>
    </div>
    <div class="row">

    </div>
  </div>
@endsection