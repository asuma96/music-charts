@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      @include('includes.auth-userdata')
      <div class="col-md-8">
        @include('message')
        {!! Form::model($media, ['url' => '/media/'. $media->id]) !!}
        {{ method_field('PATCH') }}
        @include('media.form', ['btnText' => 'Save media'])
        {!! Form::close() !!}

      </div>
    </div>
  </div>
@endsection