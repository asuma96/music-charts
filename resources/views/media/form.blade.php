<div class="form-group">
  {!! Form::label('title', 'Title') !!}
  {!! Form::text('title', null, ['class' => 'form-control input-form']) !!}
</div>
<div class="form-group">
  {!! Form::label('link_to_track', 'SoundCloud Iframe') !!}
  {!! Form::text('link_to_track', null, ['class' => 'form-control input-form']) !!}
</div>
<div class="form-group">
  {!! Form::label('genre_list', 'Extension') !!}
  {!! Form::select('genre_list[]', $genres, null, ['class' => 'form-control select-filter genre', 'multiple']) !!}
</div>
{{--<div class="form-group">--}}
  {{--{!! Form::label('year', 'Year') !!}--}}
  {{--{!! Form::number('year', date('Y'), ['class' => 'form-control input-form']) !!}--}}
{{--</div>--}}
{{--<div class="form-group">--}}
  {{--{!! Form::label('type', 'Type') !!}--}}
  {{--{!! Form::select('type', ['audio' => 'audio', 'video' => 'video'], null, ['class' => 'form-control select']) !!}--}}
{{--</div>--}}
{{--<div class="form-group">--}}
  {{--{!! Form::label('extension', 'Extension') !!}--}}
  {{--{!! Form::select('extension', ['mp3' => 'mp3', 'mp4' => 'mp4', 'other' => 'other'], null, ['class' => 'form-control']) !!}--}}
{{--</div>--}}
{{--<div class="form-group">--}}
  {{--{!! Form::label('size', 'Size') !!}--}}
  {{--{!! Form::text('size', null, ['class' => 'form-control input-form']) !!}--}}
{{--</div>--}}
{{--<div class="form-group">--}}
  {{--{!! Form::label('duration', 'Duration') !!}--}}
  {{--{!! Form::time('duration', null, ['class' => 'form-control input-form']) !!}--}}
{{--</div>--}}

{!! Form::submit($btnText, ['class' => 'btn btn-default btn-action']) !!}
