@extends('layouts.app')

@section('content')
  <div class="container page page-chart">
    <div class="row">
      <div class="col-md-12">
        <div class="title-page">
          TOP SONGS
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8">
        @if(Auth::check())
          <?php $products = Auth::user()->todayVotes->pluck('id')->toArray()?>
        @endif
        <table class="chart">
          @foreach($media as $key => $item)
            <?php $index = $key + ($media->currentPage() - 1) * $media->perPage() + 1?>
            <tr>
              <td class="@if($index<4) active @endif">
                <div class="chart-index">
                  {{$index}}
                </div>
              </td>
              <td>
                <img class="chart-avatar" src="http://placehold.it/90x90" alt="">
              </td>
              <td>
                <a class="chart-title" href="/media/{{ $item->id }}">{{ $item->title }}</a>
                {{--<time>duration: {{ $item->duration }}</time>--}}
                <a class="chart-author"
                   href="/users/{{ $item->author->id }}">{{ $item->author->name }}</a>
              </td>
              <td>
                <div class="chart-votes">
                  @if($item->votes_count < 1000)
                    {{ $item->votes_count}}
                  @elseif ($item->votes_count < 1000000)
                    {{round($item->votes_count/1000)}}K
                  @else
                    {{round($item->votes_count/1000000)}}M
                  @endif
                  @if(Auth::check())
                    @if(in_array($item->id, $products))
                      <strong>You like it!!</strong>
                    @else
                      <a href="/votes/{{ $item->id }}/attach" class="btn btn-default btn-like">Like <span
                          class="glyphicon glyphicon-thumbs-up"></span></a>

                    @endif
                  @endif
                </div>

              </td>
            </tr>
          @endforeach
        </table>


      </div>
      <div class="col-md-4">
        <div>
          {!! Form::open(['method' => 'GET', 'url' => '/media']) !!}
          {{--<div class="form-group">--}}
          {{--{!! Form::label('type', 'Select type media') !!}--}}
          {{--{!! Form::select('type', ['' => 'All', 'audio' => 'Audio', 'video' => 'Video', ], request('type'), ['class' => 'form-control select-filter', 'multiple']) !!}--}}
          {{--</div>--}}
          <div class="form-group">
            {!! Form::label('created_at', 'Select added period') !!}
            {!! Form::select('created_at', ['day' => 'Days', 'week' => 'Week', 'month' => 'Month', '' => 'All', ], request('created_at'), ['class' => 'form-control select-filter', 'multiple']) !!}
          </div>
          <div class="form-group">
            {!! Form::label('genre', 'Select genre') !!}
            {!! Form::select('genre[]', $genre, request('genre'), ['class' => 'form-control select-filter genre', 'multiple']) !!}
          </div>
          {!! Form::submit('Find!', ['class' => 'btn btn-default btn-action form-control']) !!}
          {!! Form::close() !!}
        </div>
      </div>
    </div>
    <div class="row"> {{ $media->links() }} </div>
  </div>
@endsection
