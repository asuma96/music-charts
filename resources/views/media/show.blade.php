@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading">{{ $media->title }}</div>
          <div class="panel-body">
            <p>year: {{ $media->year }}</p>
            <p>type: {{ $media->type }}</p>
            <p>extension: {{ $media->extension }}</p>
            <p>size: {{ $media->size }}</p>
            <p>duration: {{ $media->duration }}</p>
            <a href="{{ $media->link_to_track }}">link_to_track</a>
            <p>created_at: {{ date_format($media->created_at, 'd.m.Y') }}</p>
            <p>votes count: {{ $media->votes_count }}</p>
            <hr>
            <h3>tags</h3>
            @foreach($media['genres'] as $genre)
              <span style="background-color: #8f0d0b;">
                                <a style="color: #ffffff;" href="/media/genre/{{ $genre->id }}">{{ $genre->name }}</a>
                            </span>
            @endforeach
            <hr>
            <div class="alert">
              <h3>Author info</h3>
              <a href="/users/{{ $media['author']->id }}">{{ $media['author']->name }}</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
