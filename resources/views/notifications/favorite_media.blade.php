<?php $data = json_decode($notification->data); ?>
<div class="col-md-12 activity">
  <a href="/users/{{ $data->user->id }}">{{ $data->user->name }}</a> {{ $data->action }}
  <a href="/media/{{ $data->media->id }}">{{ $data->media->title }}</a>
  <?= \Carbon\Carbon::now()->diffForHumans($notification->created_at, true) ?> ago
</div>
