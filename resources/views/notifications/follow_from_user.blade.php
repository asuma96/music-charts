<?php $data = json_decode($notification->data); ?>
<div class="col-md-12 activity">
  <a href="/users/{{ $data->whoFollow->id }}">{{ $data->whoFollow->name }}</a> {{ $data->action }}
  <a href="/users/{{ $data->followUser->id }}">{{ $data->followUser->name }}</a>
  <?= \Carbon\Carbon::now()->diffForHumans($notification->created_at, true) ?> ago
</div>
