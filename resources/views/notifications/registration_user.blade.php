<?php $data = json_decode($notification->data); ?>
<div class="col-md-12 activity">
  <a href="/users/{{ $data->user->id }}">{{ $data->user->name }}</a> was registrated
  <?= \Carbon\Carbon::now()->diffForHumans($notification->created_at, true) ?> ago
</div>
