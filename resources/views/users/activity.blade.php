@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title-page">
          ACTIVITY
        </div>
      </div>
    </div>
    <div class="row">
      @include('includes.auth-userdata')
      <div class="col-md-8">
        @include('includes.tab-menu')
      @foreach($notifications as $notification)
          @include('notifications.'. snake_case(class_basename($notification->type)))
        @endforeach
        {{ $notifications->links() }}
      </div>
    </div>
  </div>
@endsection
