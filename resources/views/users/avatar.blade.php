<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
</head>
<div class="container">

    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Avatar</div>
            <div class="panel-body">
                <form method="POST" action="{{ route('upload_file') }}" enctype="multipart/form-data">
                    <input type="file" multiple name="file[]">
                    <input type="hidden" name="id" value="60">
                    <br>
                    <div class="col-md-5"></div>
                    <div class="col-md-7">
                        <button class=" btn btn-primary" type="submit">Upload</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



