@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading">Artists</div>

          <div class="panel-body">
            @foreach($users as $user)
              <h1><a href="/users/{{ $user->id }}">{{ $user->name }}</a></h1>
              @if($user->role == 'artist')
                <small>media count {{ $user->media_count }}</small>
              @endif
              <p>followers count: {{ $user->followers_count }}</p>
            @endforeach
          </div>
          {{ $users->links() }}
        </div>
      </div>
    </div>
  </div>
@endsection
