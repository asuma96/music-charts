@extends('layouts.app')

@section('content')
  <div class="container page page-chart">
    <div class="row">
      <div class="col-md-12">
        <div class="title-page">
          MESSAGES
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <table class="chart">
          @foreach($messages as $item)
            <tr>
              <td class="">
                <div class="chart-index">
                </div>
              <td>
                <img class="chart-avatar" src="http://placehold.it/90x90" alt="">
              </td>
              <td>
                <h2>{{ Html::link('/users/' . $item->recive_id, $item->recive_name)}}</h2>
                @if ($item->count_not_read)
                  new message
                @endif
              </td>
              <td>
                <a class="btn btn-default btn-action" href="">New message</a>
              </td>
            </tr>
          @endforeach
        </table>
      </div>

    </div>
    {{--<div class="row"> {{ $messages->links() }} </div>--}}
  </div>

@endsection
