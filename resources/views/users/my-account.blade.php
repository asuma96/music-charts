@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title-page">
          MY ACCOUNT
        </div>
      </div>
    </div>
    <div class="row">
      @include('includes.auth-userdata')
      <div class="col-md-8">
        @include('includes.tab-menu')
        <?php $favorites = Auth::user()->favorites()->pluck('id')->toArray() ?>
        <?php $votes = Auth::user()->todayVotes->pluck('id')->toArray()?>
        <?php $canLike = Auth::user()->todayVotes->count() >= 5 ? false : true; ?>
        @foreach($media as $item)
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-9">
                  <h3>{{ $item->title }}</h3>
                  <h5><a href="/users/{{ $item->author->id }}">{{ $item->author->name }}</a></h5>
                </div>
                <div class="col-md-3 text-right votes">
                  <div class="count-votes">
                    Votes:
                    @if($item->votes_count < 1000)
                      {{ $item->votes_count}}
                    @elseif ($item->votes_count < 1000000)
                      {{round($item->votes_count/1000)}}K
                    @else
                      {{round($item->votes_count/1000000)}}M
                    @endif
                  </div>
                  </div>
              </div>
              <iframe width="100%" height="166" scrolling="no" frameborder="no"
                      src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/{{$item->link_to_track}}&amp;color=950299&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
              @if(in_array($item->id, $favorites))
                <a class="btn btn-default btn-action" href="/favorites/{{ $item->id }}/detach">Remove from
                  favorites</a>
              @else
                <a class="btn btn-default btn-like" href="/favorites/{{ $item->id }}/attach">Add to favorites</a>
              @endif
                @if(in_array($item->id, $votes))
                  <div class="btn-liked"> <i
                      class="glyphicon glyphicon-thumbs-up"></i> Your Liked!</div>
                @else
                  @if($canLike)
                    <a href="/votes/{{ $item->id }}/attach" class="btn btn-default btn-like">
                      <i class="glyphicon glyphicon-thumbs-up"></i> Like</a>
                  @endif
                @endif
            </div>
          </div>
          <hr>
        @endforeach

        {{ $media->links() }}

      </div>
    </div>
  </div>
@endsection
