@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title-page">
          MY MEDIA
        </div>
      </div>
    </div>
    <div class="row">
      @include('includes.auth-userdata')
      <div class="col-md-8">
        @include('message')
        @include('includes.tab-menu')
        <?php $favorites = Auth::user()->favorites()->pluck('id')->toArray() ?>
        <?php $votes = Auth::user()->votes()->pluck('id')->toArray() ?>
        @foreach($media as $item)
          <div class="row">
            <div class="col-md-12">
              <h3>{{ $item->title }}</h3>
              <iframe width="100%" height="166" scrolling="no" frameborder="no"
                      src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/{{$item->link_to_track}}&amp;color=950299&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
              {{ Html::link('/media/' .  $item->id . '/edit', 'Edit', ['class'=>'btn btn-default '])}}
              <button type="button" class="btn btn-default btn-danger" data-toggle="modal" data-target="#myModal">
                Delete
              </button>

              <!-- Modal -->
              <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-body">
                      Do you have delete this song {{ $item->title }}?
                    </div>
                    <div class="modal-footer">
                      {!! Form::open(['url' => '/media/'. $item->id]) !!}
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      {{ method_field('DELETE') }}
                      {!! Form::submit('Delete', ['class' => 'btn btn-default btn-action']) !!}
                      {!! Form::close() !!}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        @endforeach
        {{ $media->links() }}
      </div>
    </div>
  </div>
@endsection
