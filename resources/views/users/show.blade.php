@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      @include('includes.auth-userdata')
      <div class="col-md-8">
        @include('includes.tab-menu')
        <form action="/users/{{ $user->id }}/send-message" method="post" style="margin: 10px 0;">
          {{ csrf_field() }}
          <textarea name="text" class="form-control border-radius-0" style="margin: 10px 0;"></textarea>
          <button class="btn btn-default btn-action">Send</button>
        </form>
        @foreach($messages as $message)
          <div class="@if($message->pivot->sender_id == Auth::user()->id) alert-success @else alert-danger @endif"  style="padding: 10px;">
            @if($message->pivot->sender_id == Auth::user()->id)
              {{ Auth::user()->name }} : <br>
            @else
              {{ $user->name }} : <br>
            @endif
            {{ $message->pivot->text }} <br> {{ $message->pivot->created_at }}
          </div>
          <hr>
        @endforeach
      </div>
    </div>
  </div>
@endsection
