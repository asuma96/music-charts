@extends('layouts.app')

@section('content')
    <div class="container page page-chart">
        <div class="row">
            <div class="col-md-12">
                <div class="title-page">
                    TOP VOTERS
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <table class="chart">
                    @foreach($users as $key => $user)
                        <?php $index = $key + ($users->currentPage() - 1) * $users->perPage() + 1?>
                        <tr>
                            <td class="@if($index<4) active @endif">
                                <div class="chart-index">
                                    {{$index}}
                                </div>
                            <td>
                                <img class="chart-avatar" src="http://placehold.it/90x90" alt="">
                            </td>
                            <td>
                                <h1>{{ Html::link('/users/' .  $user->id, $user->name)}}</h1>
                            </td>
                            <td>
                                <a class="btn btn-default btn-like" href="">Unfollow</a>
                            </td>
                            <td>
                                <div class="chart-votes">
                                    @if($user->votes_count < 1000)
                                        {{ $user->votes_count}}
                                    @elseif ($user->votes_count < 1000000)
                                        {{round($user->votes_count/1000)}}K
                                    @else
                                        {{round($user->votes_count/1000000)}}M
                                    @endif
                                </div>

                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>

            <div class="col-md-4">
                <div>
                    {!! Form::open(['method' => 'GET', 'url' => '/top-voters']) !!}
                    <div class="form-group">
                        {!! Form::label('top', 'Select added period') !!}
                        {!! Form::select('top', ['day' => 'Days', 'week' => 'Week', 'month' => 'Month', '' => 'All', ], request('created_at'), ['class' => 'form-control select-filter', 'multiple']) !!}
                    </div>
                    {{--<div class="form-group">--}}
                    {{--{!! Form::label('genre', 'Select genre') !!}--}}
                    {{--{!! Form::select('genre[]', $genre, request('genre'), ['class' => 'form-control select-filter genre', 'multiple']) !!}--}}
                    {{--</div>--}}
                    {!! Form::submit('Find!', ['class' => 'btn btn-default btn-action form-control']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="row"> {{ $users->links() }} </div>
    </div>

@endsection
