<?php

Route::get('media/genre/{id}', 'GenreController@products');
Route::get('artists', 'UserController@artists');
Route::get('produsers', 'UserController@produsers');
Route::get('top-voters', 'UserController@topVoters');
Route::get('media', 'MediaController@index');
Route::get('/', 'MediaController@index');
Auth::routes();
Route::get('search', 'UserController@search');
Route::get('media-search', 'MediaController@search');
Route::get('likes', 'MediaController@likes');
Route::post('edit', 'UserController@edit');
Route::get('edit', 'UserController@editProfile');

Route::group(['middleware' => 'auth'], function () {
    Route::get('favorites', 'UserController@favoritesList');
    Route::get('users/{id}', 'UserController@show');
    Route::get('my-account', 'UserController@myAccount');
    Route::get('activity', 'UserController@activity');
    Route::get('media/create', 'MediaController@create');
    Route::get('media/{media}/edit', 'MediaController@edit');
    Route::post('media', 'MediaController@store');
    Route::patch('media/{media}', 'MediaController@update');
    Route::delete('media/{media}', 'MediaController@destroy');
    Route::get('my-media', 'UserController@myMedia');
    Route::get('unread', 'UserController@unreadMessage');
    Route::get('{relation}/{id}/{action}', 'UserController@sync');
    Route::post('users/{user}/send-message', 'UserController@sendMessage');

});

Route::get('/avatar', function () {
    return view('/users/avatar');
});


Route::get('media/{id}', 'MediaController@show');

