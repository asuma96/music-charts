const { mix } = require('laravel-mix');

mix.js('resources/assets/js/app.js', 'public/js')
  .sass('resources/assets/sass/app.scss', 'public/css')
  .copy('node_modules/bootstrap-sass/assets/fonts', 'public/fonts/')
  .copy('resources/assets/fonts', 'public/fonts/')
  .copy('resources/assets/img', 'public/img/')